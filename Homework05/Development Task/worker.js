function worker(n, k) {
    let p = n/3;
    let w = Math.pow(p,k);
    return w;
}

function main(params) {
    let w = worker(params.N, params.k);

    let placements_per_function = Math.pow(params.N, params.N) / w;

    let placements_from = [];
    for (let i = 0; i < w; i++) {
        placements_from.push([i * placements_per_function, (i + 1) * placements_per_function])
    }


    let placements_from_first = placements_from.slice(0, ((placements_from.length / 3)));
    let placements_from_second = placements_from.slice(((placements_from.length / 3)), 2 * ((placements_from.length / 3)));
    let placements_from_third = placements_from.slice(2* ((placements_from.length / 3)), placements_from.length);

    return {
        "workers_first": placements_from_first.length,
        "workers_second": placements_from_second.length,
        "workers_third": placements_from_third.length,
        "placements_per_function": placements_per_function,
        "placements_from_first": placements_from_first,
        "placements_from_second": placements_from_second,
        "placements_from_third": placements_from_third
    };
}