function main(params) {
    let sum = 0;
    params.solutions.forEach(element => {
        sum += element;
    });

    return {
        solutions: sum
    };
}