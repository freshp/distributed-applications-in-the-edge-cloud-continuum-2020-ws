function worker(n, k) {
    let p = n/3;
    let w = Math.pow(p,k);
    return w;
}

function main(params) {
    let w = worker(params.N, params.k);

    let placements_per_function = Math.pow(params.N, params.N) / w;

    let placements_from = [];
    for (let i = 0; i < w; i++) {
        placements_from.push([i * placements_per_function, (i + 1) * placements_per_function])
    }

    return {
        "workers": w,
        "placements_from": placements_from,
        "placements_per_function": placements_per_function
    };
}