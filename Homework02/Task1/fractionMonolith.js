function acceptable(num_queens, queen_rows) {
    for (let i = 0; i < num_queens; i++) {
        for (let j = i + 1; j < num_queens; j++) {
            if (queen_rows[i] == queen_rows[j]) {
                return false;
            }

            if (queen_rows[i] - queen_rows[j] == i - j || queen_rows[i] - queen_rows[j] == j - i) {
                return false;
            }
        }
    }

    return true;
}

function fraction({ from, to, num_queens }) {
    return new Promise(resolve => {
        let solution = 0;

        for (let iter = from; iter < to; iter++) {
            let code = iter;
            let queen_rows = new Array(num_queens);
            for (let i = 0; i < num_queens; i++) {
                queen_rows[i] = code % num_queens;
                code = Math.floor(code / num_queens);
            }
            if (acceptable(num_queens, queen_rows)) {
                solution += 1;
            }
        }
        resolve(solution);
    });
}
let n = 8;
let max_iter = 1;
for (let i = 0; i < n; i++) {
    max_iter *= n;
}


async function main(params) {
    return {
        result: await fraction({
            num_queens: 8,
            from: 0,
            to: max_iter
        })
    }
}
