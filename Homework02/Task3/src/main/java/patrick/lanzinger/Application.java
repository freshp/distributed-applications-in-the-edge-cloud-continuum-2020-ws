package patrick.lanzinger;

import com.google.gson.JsonObject;
import jFaaS.Gateway;
import jFaaS.invokers.FaaSInvoker;
import jFaaS.invokers.HTTPGETInvoker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Application {
    private static final String URL_NQUEENS = "https://eu-gb.functions.appdomain.cloud/api/v1/web/patrick.lanzinger%40student.uibk.ac.at_dev/default/nqueens-h02.json";
    private static final String URL_NQUEENS_FAAS = "https://eu-gb.functions.appdomain.cloud/api/v1/web/patrick.lanzinger%40student.uibk.ac.at_dev/default/nqueens-faas.json";
    public static void main(String[] args) {
        FaaSInvoker faaSInvoker = new HTTPGETInvoker();

        //Map<String, Object> input = new HashMap<String, Object>();
        //input.put("n", args[0]);

        JsonObject result = null;
        long averageTime = 0;
        try {
            for (int i= 0; i < 20; i++) {
                long startTime = System.currentTimeMillis();
                result = faaSInvoker.invokeFunction(URL_NQUEENS_FAAS, new HashMap<>());
                long endTime = System.currentTimeMillis();
                averageTime += endTime - startTime;
                System.out.println(endTime - startTime);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        System.out.println("Average time: " + averageTime / 20 + "ms");
    }
}
