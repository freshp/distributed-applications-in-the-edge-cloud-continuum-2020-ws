// l require(./nqueens.js as queens) name(nqueens) return(result)
let queens = require('./nqueens')


let n = 8;
let max_iter = 1;
for (let i = 0; i < n; i++) {
    max_iter *= n;
}

let param = {
    n: 8,
    from: 0,
    to: max_iter
}

let result = await queens.fraction(param)
// lend