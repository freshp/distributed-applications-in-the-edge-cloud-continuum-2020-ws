'use strict';

// l require(./external.js as external)
var external = require('./external');
// lend

exports.fraction = async function (ev) {
    var from = parseInt(ev.from);
    var to = parseInt(ev.to);
    var num_queens = parseInt(ev.num_queens);
    var solutions = 0;
    for(var iter = from; iter < to; iter++){
        var code = iter;
        var queen_rows = [];
        for(var i = 0; i < num_queens; i++){
            queen_rows[i] = code % num_queens;
            code = Math.floor(code/num_queens);
        }
        if(external.acceptable(num_queens, queen_rows)){
            solutions += 1;
        }
    }
    var result = { "solutions": solutions };
    return result;
}

