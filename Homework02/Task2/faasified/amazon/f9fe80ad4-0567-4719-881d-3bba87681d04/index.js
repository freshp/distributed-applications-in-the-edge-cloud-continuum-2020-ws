const queens = require("./nqueens.js")

module.exports = async (event) => {
  let queens = require("./nqueens")
}

module.exports.handler = async (event, context) => {
  const userFunc = module.exports
  let res
  try {
    res = await userFunc(event)
  } catch (e) {
    context.fail(e)
  }
  context.succeed(res)
}
