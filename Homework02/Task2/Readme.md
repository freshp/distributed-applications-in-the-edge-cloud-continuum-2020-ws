# Steps for ibm-cloud-cli

## Login
```
$ ibmcloud login -a cloud.ibm.com -o "patrick.lanzinger@student.uibk.ac.at" -s "dev"
```

## Create Function
```
$ ibmcloud wsk action create nqueens-faas \--kind nodejs:12 nqueens.zip 
```

## Faasify function
```
$ x2faas --fpath main.js --linenum 0 --outpath faasified --provider ibm
```