# Exercise 1
## Task 4

### London
The first execution had an average RTT of 11750ms.
The second execution had an average RTT of 112222ms.


### Tokyo
The first execution had an average RTT of 9561ms.
The second execution had an average RTT of 9531ms.

### Observations

I think the one in tokyo was faster because of the time when I run the test. Probably 
that datacenter had a very low load and could complete the task more quickly. 

## Task 5

With maximum memory the RTT is 11280mx. 

### Observation

Adding more memory to the problem does not increase performance because the problem 
is not memory bound.