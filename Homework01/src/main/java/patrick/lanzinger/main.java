package patrick.lanzinger;

import com.google.gson.JsonObject;
import jFaaS.Gateway;
import jFaaS.invokers.FaaSInvoker;
import jFaaS.invokers.HTTPGETInvoker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class main {
    private static final String URL = "https://eu-gb.functions.appdomain.cloud/api/v1/web/patrick.lanzinger%40student.uibk.ac.at_dev/default/nqueens.json";
    private static final String URL_TOKYO = "https://jp-tok.functions.appdomain.cloud/api/v1/web/e9cb12d4-2a64-4aa8-b859-9c5f3ad6c546/default/nqueens.json";
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("You have to provide to parameters");
            System.out.println("The first one is N the second one is K");
            System.out.println("homework01 8 10");
            return;
        }



        FaaSInvoker faaSInvoker = new HTTPGETInvoker();

        Map<String, Object> input = new HashMap<String, Object>();
        input.put("n", args[0]);

        JsonObject result = null;
        long averageTime = 0;
        try {
            for (int i= 0; i < Integer.valueOf(args[1]); i++) {
                long startTime = System.currentTimeMillis();
                result = faaSInvoker.invokeFunction(URL, input);
                long endTime = System.currentTimeMillis();
                averageTime += endTime - startTime;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(result);
        System.out.println("Average time: " + averageTime / Integer.valueOf(args[1]));
    }
}
