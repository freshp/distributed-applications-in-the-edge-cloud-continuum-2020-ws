'use strict';

let num_queens = -1;

function acceptable(queen_rows){
    for(let i = 0; i < num_queens; i++){
        for(let j = i + 1; j < num_queens; j++){
            if(queen_rows[i] == queen_rows[j]){
                return false;
            }
            if(queen_rows[i] - queen_rows[j] == i - j || queen_rows[i] - queen_rows[j] == j - i){
                return false;
            }
        }
    }
    return true;
}


function fraction(n, from, to){
    num_queens = n;
    let solutions = 0;
    for(let iter = from; iter < to; iter++){
        let code = iter;
        let queen_rows = [];
        for(let i = 0; i < num_queens; i++){
            queen_rows[i] = code % num_queens;
            code = Math.floor(code/num_queens);
        }
        if(acceptable(queen_rows)){
            solutions += 1;
        }
    }
    return solutions;
}


function main(params) {
    if (params.n != null) {
        let max_iter = 1;
        for (let i = 0; i < params.n; i++) {
            max_iter *= params.n;
        }


        return {
            result: fraction(params.n, 0, max_iter)
        }
    } else {
        return {
            result: 0
        }
    }

}
